'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsuariosSchema extends Schema {
  up () {
    this.create('usuarios', (table) => {
      table.increments()
      table.string('nickname').notNullable().unique()
      table.string('name').notNullable()
      table.string('last_name').notNullable()
      table.string('password').notNullable()
      table.string('email').notNullable().unique()
      table.enum('rol',['Admin','Operador','Administrativo']).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('usuarios')
  }
}

module.exports = UsuariosSchema
