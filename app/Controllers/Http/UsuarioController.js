'use strict'
const { validateAll } = use('Validator')
const Users = use('App/Models/Usuario');
/**
 * Resourceful controller for interacting with usuarios
 */
class UsuarioController {

  async index({ request, response }) {

    const validation = await validateAll(request.all(), { page: 'required|number' })
    if (validation.fails()) {
      return response.notFound({ success: false, message: validation.messages() })
    }
    const data = await Users.query().paginate(request.input('page'), 10)
    const resp = data.rows.length === 0
      ? response.notFound({ success: false, message: 'Sin registro de usuarios' })
      : response.ok({ success: true, data: data })
    return resp

  }

  async store({ request, response }) {

    const rules = {
      nickname: 'required|unique:usuarios',
      name: 'required',
      last_name: 'required',
      password: 'required',
      email: 'required|email|unique:usuarios',
      rol: 'required|in:Admin,Operador,Administrativo',
    }
    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.notFound({ success: false, message: validation.messages() })
    }

    const User = request.only([
      'nickname',
      'name',
      'last_name',
      'password',
      'email',
      'rol'
    ])

    const usuario = new Users()
    usuario.fill(User)
    await usuario.save()
    response.created({ success: true, message: 'Usuario registrado', data: usuario })
  }

  async show({ params, response }) {
    const user = await Users.find(params.id)
    if (!user) {
      return response.accepted({ success: false, message: 'No hay registro con el id: ' + params.id })
    }
    return response.ok({ success: true, data: user })
  }

  async update({ params, request, response }) {

    const user = await Users.find(params.id)
    if (!user) {
      return response.notFound({ success: false, message: 'No hay registro con el id: ' + params.id })
    }
    const rules = {
      nickname: 'unique:usuarios',
      email: 'email|unique:usuarios'
    }
    const validation = await validateAll(request.all(), rules)
    if (validation.fails()) {
      return response.notFound({ success: false, message: validation.messages() })
    }

    const User = request.only([
      'nickname',
      'name',
      'last_name',
      'password',
      'email',
      'rol'
    ])
    user.merge(User)
    await user.save()
    return response.created({ success: true, data: user })
  }

  async destroy({ params, response }) {
    const user = await Users.find(params.id)
    if (!user) {
      return response.notFound({ success: false, message: 'No hay registro con el id: ' + params.id })
    }
    user.delete()
    return response.accepted({ success: true, message: 'se elmino ususario', data: user })
  }

  async filter({ request, response }) {

    const validation = await validateAll(request.all(), { rol: 'required' })
    if (validation.fails()) {
      return response.notFound({ success: false, message: validation.messages() })
    }
    const data = await Users.query()
      .where('rol', request.input('rol')).fetch()

    const resp = data.rows.length === 0
      ? response.notFound({ success: false, message: 'no se encontro usuario con rol ' + request.input('rol') })
      : response.ok({ success: true, data: data })
    return resp
  }
}

module.exports = UsuarioController
